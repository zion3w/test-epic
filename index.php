<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// for big .csv files
set_time_limit(0);

// for debug
function dd($code){
	echo '<hr><div><pre><code>';
	var_dump($code);
	echo '</code></pre></div><hr>';
	die;
}
// for debug/




require (__DIR__ . '/db-config.php');

// db

$host = $config['db_host'];
$user = $config['db_user'];
$pass = $config['db_pass'];
$dbName = $config['db_name'];



require (__DIR__ . '/classes/db.php');
$db = new DbController($host,$user,$pass,$dbName);

// db/


// vars
$pathToCsvFile = __DIR__ . '/csv/10.csv';

$dbTableName = 'users';

$uniqueUserColumns = 'email,card,phone';

$uniqueUserColumsArr = explode(',', $uniqueUserColumns);
$uniqueUserColumsArr = array_fill_keys($uniqueUserColumsArr,'');
// vars/

// funcs

function arrFromMySQLToNormalArr($arrFromMySQL) {

	$cleanArr = [];

	foreach ($arrFromMySQL as $key => $value) {

		if (is_array($value)) {
			$cleanArr[$key] = arrFromMySQLToNormalArr($value);
		} else {
			$value = stripcslashes(html_entity_decode($value));
			// check if a string is json encoded array
			if (is_object(json_decode($value))) {
				$cleanArr[$key] = arrFromMySQLToNormalArr(json_decode($value, true));
			} else {
				$cleanArr[$key] = stripcslashes($value);
			}
		}

	}
	return $cleanArr;
}
// decode eitities, strip backslashes
function normalizeArrWithStrings($arrFromMySQL) {
	$cleanArr = [];
	foreach ($arrFromMySQL as $key => $value) {

		if (is_array($value)) {
			$cleanArr[$key] = normalizeArrWithStrings($value);
		} else {
			$value = stripcslashes(html_entity_decode($value));
			$cleanArr[$key] = stripcslashes($value);
		}

	}
	return $cleanArr;
}
// funcs/


$csv = array_map('str_getcsv', file($pathToCsvFile, FILE_SKIP_EMPTY_LINES));
$keys = array_shift($csv);

foreach ($csv as $i=>$row) {
	$csv[$i] = array_combine($keys, $row);

	$userDataArr = $csv[$i];

	unset($userDataArr['id']); // remove id from user arr
	unset($userDataArr['parent_id']); // remove parent_id from user arr

	// fill unique user values
	$uniqueUserColumsArr = array_intersect_key($userDataArr, $uniqueUserColumsArr);

	// get 'id' of first occurrence of unique user fields
	foreach ($uniqueUserColumsArr as $key => $value) {

		$sameUserColumsId = $db->query("SELECT id FROM $dbTableName WHERE $key='$value' GROUP BY 'id'");
		// in case of error in 'Uncaught Exception: MySQL query error: Expression #1 of SELECT list is not in GROUP BY clause and contains nonaggregated column'
		// use it
		// $sameUserColumsId = $db->query("SELECT id FROM $dbTableName WHERE $key='$value'");

		if (!empty($sameUserColumsId)) {
			$userDataArr['parent_id'] = $sameUserColumsId[0]['id'];
			break;
		}

	}

	// create new user in mysql table
	$db->insertInto($dbTableName, $userDataArr);

}



// show all users in json

$allColumnsArr = $db->query("SELECT * FROM $dbTableName");
$allColumnsRealArr = arrFromMySQLToNormalArr($allColumnsArr);

header('Content-Type: application/json');
echo json_encode($allColumnsRealArr);
?>
