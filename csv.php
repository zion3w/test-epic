<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// for big .csv files
set_time_limit(0);

// for debug
function dd($code){
	echo '<hr><div><pre><code>';
	var_dump($code);
	echo '</code></pre></div><hr>';
	die;
}
// for debug/

if (!isset($_POST["submit"])) {
	die;
}



// vars

$targetDir =  __DIR__ . '/csv/';
$targetFile = $targetDir . basename($_FILES['fileToUpload']['name']);

$csvFile = $_FILES['fileToUpload']['name'];
$pathToCsvFile = $targetFile;

// $csvFile = 'users.csv';
// $pathToCsvFile = __DIR__ . '/csv/' . $csvFile;

$uniqueUserColumns = 'email,card,phone';

$uniqueUserColumsArr = explode(',', $uniqueUserColumns);
$uniqueUserColumsArr = array_fill_keys($uniqueUserColumsArr,'');
// vars/


if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $targetFile)) {

	// func
	function array2csv($data, $delimiter = ',', $enclosure = '"', $escape_char = "\\")
	{
		$f = fopen('php://memory', 'r+');
		foreach ($data as $item) {
			fputcsv($f, $item, $delimiter, $enclosure, $escape_char);
		}
		rewind($f);
		return stream_get_contents($f);
	}

	// func/


	$csvArr = array_map('str_getcsv', file($pathToCsvFile, FILE_SKIP_EMPTY_LINES));
	$csvArrKeys = array_shift($csvArr);

	$usersArr = [];

	$breakVar = '';

	foreach ($csvArr as $i=>$row) {

		$csvArr[$i] = array_combine($csvArrKeys, $row);

		$userDataArr = $csvArr[$i];

		// fill unique user keys with current user values
		$uniqueUserColumsArr = array_intersect_key($userDataArr, $uniqueUserColumsArr);
		$uniqueUserColumsArrVals = array_values($uniqueUserColumsArr);


		foreach ($uniqueUserColumsArr as $key => $value) {

			foreach ($usersArr as $usersArrKey => $usersArrValue) {

				if (in_array($value, $usersArrValue)) {
					$userDataArr['parent_id'] = $usersArrValue['id'];
					$breakVar = 1;
					break;
				}

			}

			if ($breakVar) {
				$breakVar = '';
				break;
			}

		}


		$usersArr[$i] = $userDataArr;
	}

	// sort arr
	$parent_id = array_column($usersArr, 'parent_id');
	array_multisort($parent_id, SORT_ASC, $usersArr);

	// remove uploaded file
	unlink($targetFile);

	// dd($usersArr);

	// set response in .csv
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=$csvFile");
	header("Pragma: no-cache");
	header("Expires: 0");


	echo implode(',' ,$csvArrKeys);
	echo "\n";
	echo array2csv($usersArr);

} else {
	die('Error uploading your file.');
}

?>
