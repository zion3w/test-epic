# import users from csv file and process into csv file or into mysql table

### find duplicates in unique user fields and set first duplicate user 'id' as 'parent_id'

version with csv to csv https://test.egyptian-pharaoh.xyz/csv.html (can process 100k users really fast). `csv` in `csv` out

frontend `csv.html` and backend `csv.php`

can download csv files with initially added users:
https://test.egyptian-pharaoh.xyz/csv/10.csv - 10 records
https://test.egyptian-pharaoh.xyz/csv/10k.csv - 10 000 records
https://test.egyptian-pharaoh.xyz/csv/100k.csv - 10 000 records

---

can check here version with mysql https://test.egyptian-pharaoh.xyz (slow with 10k+ users in one file, can faster, but have no time for this). as a response you will see json object with all users in mysql database.

every time when you visit https://test.egyptian-pharaoh.xyz/ users from https://test.egyptian-pharaoh.xyz/csv/10.csv file will be added to mysql db
