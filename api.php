<?php

/*
// get data from MySQL tables
https://test.egyptian-pharaoh.xyz/api.php/?auth=epic@2020&format=json&table=users&columns=tel,name
https://test.egyptian-pharaoh.xyz/api.php/?auth=epic@2020&format=csv&table=users&columns=*

params:
auth=AuthKey - required key for authorization
format=csv - get response as .csv document
format=json - get response as .json
table=<TABLE_NAME> - database table name
columns=<TABLE_COLUMN1,TABLE_COLUMN2,TABLE_COLUMN3> - database table columns
columns=* - get all columns from table
examples:
get all columns(*) from "users" table as document in "csv" format
/api.php/?auth=epic@2020&format=csv&table=users&columns=*
get "tel,name" columns from "users" table in "json" format
/api.php/?auth=epic@2020&format=json&table=users&columns=tel,name
*/

if (!isset($_GET['auth']) || $_GET['auth'] !== 'epic@2020') {
	http_response_code(401);
	die('authentication failed');
}


set_time_limit(0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// for debug
function dd($code){
	echo '<hr><div><pre><code>';
	var_dump($code);
	echo '</code></pre></div><hr>';
	die;
}


// helper funcs

function arrFromMySQLToNormalArr($arrFromMySQL) {

	$cleanArr = [];

	foreach ($arrFromMySQL as $key => $value) {

		if (is_array($value)) {
			$cleanArr[$key] = arrFromMySQLToNormalArr($value);
		} else {
			$value = stripcslashes(html_entity_decode($value));
			// check if a string is json encoded array
			if (is_object(json_decode($value))) {
				$cleanArr[$key] = arrFromMySQLToNormalArr(json_decode($value, true));
			} else {
				$cleanArr[$key] = stripcslashes($value);
			}
		}

	}
	return $cleanArr;
}
// decode eitities, strip backslashes
function normalizeArrWithStrings($arrFromMySQL) {
	$cleanArr = [];
	foreach ($arrFromMySQL as $key => $value) {

		if (is_array($value)) {
			$cleanArr[$key] = normalizeArrWithStrings($value);
		} else {
			$value = stripcslashes(html_entity_decode($value));
			$cleanArr[$key] = stripcslashes($value);
		}

	}
	return $cleanArr;
}

function array2csv($data, $delimiter = ',', $enclosure = '"', $escape_char = "\\")
{
	$f = fopen('php://memory', 'r+');
	foreach ($data as $item) {
		fputcsv($f, $item, $delimiter, $enclosure, $escape_char);
	}
	rewind($f);
	return stream_get_contents($f);
}



// helper funcs/



require (__DIR__ . '/db-config.php');



$host = $config['db_host'];
$user = $config['db_user'];
$pass = $config['db_pass'];
$dbName = $config['db_name'];

require (__DIR__ . '/classes/db.php');
$db = new DbController($host,$user,$pass,$dbName);



$dbTableName = 'users';

$columns = 'tel';



if (isset($_GET['table'])) {
	$dbTableName = htmlentities($_GET['table']);
}
if (isset($_GET['columns'])) {
	$columns = htmlentities($_GET['columns']);
}


$allColumnsArr = $db->query("SELECT $columns FROM $dbTableName");
$allColumnsArrNormalizedStrings = normalizeArrWithStrings($allColumnsArr);
$allColumnsRealArr = arrFromMySQLToNormalArr($allColumnsArr);


if (isset($_GET['format']) && $_GET['format'] === 'json') {

	// set response in .json
	header('Content-Type: application/json');
	echo json_encode($allColumnsRealArr);

} elseif (isset($_GET['format']) && $_GET['format'] === 'csv') {

	// set response in .csv
	$fileName = $dbTableName . '_' . $columns;
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=$fileName.csv");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo array2csv($allColumnsArrNormalizedStrings);

}
